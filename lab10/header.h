# ifndef __HEADER_H_
# define __HEADER_H_


# include "mpi.h"
# include <stdio.h>
# include <time.h>
# include <stdlib.h>
# include <unistd.h>
# include <string.h>
#include <sys/time.h>


# define SONDAJ 0
# define ECOU 1


/*
0 1
0 2
1 3
1 4
1 5
2 6
2 7
4 8
4 9
7 10

6 7
*/


char * myTime ( ) {

	char *cBuffer = (char*) calloc (100,sizeof(char));
    time_t zaman;
    struct tm *ltime;
    static struct timeval _t;
    static struct timezone tz;

    time(&zaman);
    ltime = (struct tm *) localtime(&zaman);
    gettimeofday(&_t, &tz);

    strftime(cBuffer,100,"%d.%m.%y %H:%M:%S",ltime);
    sprintf(cBuffer, "%s.%d", cBuffer,(int)_t.tv_usec);

	return cBuffer;
	
}


# endif
