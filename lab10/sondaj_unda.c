# ifndef __SONDAJ_UNDA_
# define __SONDAJ_UNDA_

# include "header.h"

int main (int argc, char *argv[])  {

	int rank, rc, tag=1;
	MPI_Status Stat;
	int i,j;  
	int numtasks;
	int init = 0;
	int parinte, sons, son;

	char a[] = "out";
	char *nume;
	FILE *fp;
	FILE *fin;
	
	int N, M;
	
	
	
	// adaug si muchia 6-7 astfel incat sa am un ciclu
	
	
	int X[11] = { 0 , 0 , 1, 1 , 1 , 2 , 2 , 4 , 4 , 7, 6 };
	int Y[11] = { 1 , 2 , 3 , 4, 5, 6 ,7 ,8 ,9 , 10 , 7 };
	
	int *matrix = (int*) calloc ((1+11*11),sizeof(int));
	int *partial = (int*) calloc ((1+11*11),sizeof(int));
	int *recv = (int*) calloc ((1+11*11),sizeof(int));
	
	
	for ( i = 0 ; i < 11 ; i ++ ) {
	
		matrix[ X[i] * 11 + Y[i] + 1 ] = 1;
		matrix[ Y[i] * 11 + X[i] + 1 ] = 1;
	
	}
	
	matrix [1+ 5 *11 + 6 ] = matrix [ 1 + 6 * 11 + 5 ] = 1;
	
	
    MPI_Init(&argc,&argv);
    MPI_Comm_size(MPI_COMM_WORLD, &numtasks);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

	if ( rank == 0 ) {
	
	
	# ifdef DEBUG
	
		printf ( "\nStart:");
		printf ( "\n");
		for ( i = 0 ; i <11 ; i ++  ) {
	
			for ( j = 0 ; j < 11 ; j ++ ) 
			
				printf ( "%d " , matrix[ i * 11 + j + 1] );
			
			printf ( "\n");
		
		}
		
			printf ( "\n");
			printf ( "\n");
	
	# endif
	
	}

		// fiecare proces pastreaza doar linia sa
		for ( i = 0 ; i < 11 ; i ++ ) {
		
			if ( i != rank ) {
			
				for ( j = 0 ; j < 11 ; j ++ ) {
				
					matrix[i*11 + j + 1 ] = 0;
				
				}
			
			} else { 
			
		
			char numar[10];
	  		sprintf ( numar, "%d", rank );
	  		nume = strcat ( a , numar );
	  		fp = fopen ( nume , "w" );
	  		
			}
		
		}
	
		// acesta este initiatorul;
		if ( init == rank ) {
		
			# ifdef DEBUG 
			
				for ( i = 0 ; i <11 ; i ++  ) {
	
					printf ( "%d : " , i );
					for ( j = 0 ; j < 11 ; j ++ ) 
			
						printf ( "%d " , matrix[ 1 + i * 11 + j ] );
			
					printf ( "\n");
		
				}
			
				printf ( "\n\n");
			
			# endif

		
			// initiatorul trimite sondaj la fii sai.
			
			sons = 0;
			for ( j = 0 ; j < 11 ; j ++ ) {
			
				// acesta este unu fiu al initiatorului care va primi sondaj
				// si apoi va astepta ecou;
				if ( matrix[ rank * 11 + j + 1 ] == 1 ) {
				
					fprintf ( fp , " %s ... Rank %d ii trimite sondaj lui %d...\n" , myTime () , rank , j );
					memset ( partial , 0 , ( 11 * 11 + 1 )* sizeof(int) );
					partial[0] = SONDAJ;
					sons ++;
					rc = MPI_Send(partial, 11*11 + 1 , MPI_INT , j , tag, MPI_COMM_WORLD);

				}

			}
			
			// TODO : Initiatorul asteapta reply de la fii sai

			for ( j = 0 ; j < sons ; j ++ ) {
			
				memset ( recv , 0 , ( 11 * 11 + 1 )* sizeof(int) );
				rc = MPI_Recv(recv , 11 * 11 + 1 , MPI_INT , MPI_ANY_SOURCE , tag, MPI_COMM_WORLD, &Stat);
				son = Stat.MPI_SOURCE;
				
				if ( matrix[11*rank + 1 + son ] != 1 ) {
				
					fprintf ( fp,"%s -- Error... rank %d nu este vecin cu %d...si tip este %d\n", myTime () , rank , son, recv[0] );
				
				}
				
				if ( recv[0] == ECOU ) {
				
					fprintf ( fp , "%s ... Rank %d a primit ecou de la %d... \n", myTime () , rank , son );
					
					//TODO : Combin rezultatele :
					
					for ( i = 1 ; i <= 11*11 ; i ++ ) {

							if ( recv[i] == 1 || matrix[i] == 1 ) {

								matrix[i] = 1;

							}

						}
				
				} else if ( recv[0] == SONDAJ ) {
				
					sons ++;
					matrix[11*rank + 1 + son ] = 0 ;
					fprintf ( fp ,"%s ... Rank %d a primit sondaj de la %d... Invalidez legatura %d - %d ... si rank %d ii trimite ecou gol lui %d ...\n" , myTime () , rank , son , rank , son , rank , son );
									
					memset ( partial , 0 , ( 11 * 11 + 1 )* sizeof(int) );
					partial[0] = ECOU;
					rc = MPI_Send(partial, 11*11 + 1 , MPI_INT , son , tag, MPI_COMM_WORLD);
				
				
				}
			
			}
			
			// TODO : Afisez rezultatul final
			
			for ( i = 0 ; i <11 ; i ++  ) {
	
					printf ( "%d : " , i );
					for ( j = 0 ; j < 11 ; j ++ ) 
			
						printf ( "%d " , matrix[ 1 + i * 11 + j ] );
			
					printf ( "\n");
		
				}
			
			printf ( "\n\n");
			
		} else {
		
			// TODO : Astept un sondaj de la parinte
			
			memset ( recv , 0 , ( 11 * 11 + 1 )* sizeof(int) );
			rc = MPI_Recv(recv , 11 * 11 + 1 , MPI_INT , MPI_ANY_SOURCE , tag, MPI_COMM_WORLD, &Stat);
			parinte = Stat.MPI_SOURCE;
			
			if ( recv[0] == SONDAJ ) {
			
				fprintf ( fp ,"%s... Echo...rank %d...am primit sondaj de la %d...\n",myTime (), rank , parinte );
			
			} else if ( recv[0] == ECOU ) {
			
				fprintf ( fp , "%s ... Echo...rank %d...am primit ecou de la  %d...\n", myTime () , rank , parinte );
			
			}
			
			
			// TODO : Trimit mesaj la toti fii mei
			
			sons = 0;
			
			for ( j = 0 ; j < 11 ; j ++ ) {

				if ( matrix[ 11 * rank + j + 1] == 1 &&  j != parinte ) {
				
					fprintf (fp, "%s ... Rank %d ii trimite sondaj lui %d...\n" ,myTime () , rank , j );
					// numar cati fii am
					sons ++;
					
					// TODO : trimit sondaj la fii
					memset ( partial , 0 , ( 11 * 11 + 1 )* sizeof(int) );
					partial[0] = SONDAJ;
					rc = MPI_Send(partial, 11*11 + 1 , MPI_INT , j , tag, MPI_COMM_WORLD);
				
				}
				
			}
			
			// TODO : Astept mesaj de la fii mei
			
			if ( sons != 0 ) {
			
				for ( j = 0 ; j < sons ; j ++ ) {
			
					memset ( recv , 0 , ( 11 * 11 + 1 )* sizeof(int) );
					rc = MPI_Recv(recv , 11 * 11 + 1 , MPI_INT , MPI_ANY_SOURCE , tag, MPI_COMM_WORLD, &Stat);
					son = Stat.MPI_SOURCE;
				
				
					if ( recv[0] == ECOU ) {
				
						fprintf ( fp , "%s ... Rank %d a primit ecou de la %d... \n" ,myTime (), rank , son );
						//TODO : Compun rezultatele
						
						# ifdef DEBUG
						
							if (  rank == 7  && son == 10 ) {
							
								printf ( "--------------%d\n" , recv[11*10+1+7] );
							
							} 
						
						# endif
						
						for ( i = 1 ; i <= 11*11 ; i ++ ) {

							if ( recv[i] == 1 || matrix[i] == 1 ) {

								matrix[i] = 1;

							}

						}
						
						
				
					} else if ( recv[0] == SONDAJ ) {
				
						matrix[11*rank + 1 + son ] = 0 ;
						sons ++;
						fprintf ( fp, "%s ... Rank %d a primit sondaj de la %d... Invalidez legatura %d - %d ... si rank %d ii trimite ecou gol lui %d ...\n" ,myTime (), rank , son , rank , son , rank , son );
									
						memset ( partial , 0 , ( 11 * 11 + 1 )* sizeof(int) );
						partial[0] = ECOU;
						rc = MPI_Send(partial, 11*11 + 1 , MPI_INT , son , tag, MPI_COMM_WORLD);

					}
			
				}
			
			} 
			
			// TODO : trimit ecou la parinte 
			matrix[0] = ECOU;
			
			# ifdef DEBUG 
			
				if (  rank == 10 ) {
			
					printf ( "+++++++++++++++++++++++");
					printf ( "%d\n", matrix[10*11+1+7] );
			
				}
			
			# endif
			
			rc = MPI_Send(matrix, 11*11 + 1 , MPI_INT , parinte , tag, MPI_COMM_WORLD);
				
		}
		
		fclose (fp);

	MPI_Finalize();

	free ( matrix );
	free ( partial );
	
	return 0;	
}

# endif
