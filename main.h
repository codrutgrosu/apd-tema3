/*
 * 
 * Codrut Cristian Grosu
 * codrut.cristian.grosu@gmail.com
 * Anul III - Seria CA - Grupa 333
 * Tema 3 - APD
 * 
 */

# ifndef __MAIN_H_
# define __MAIN_H_

# include "keep.h"
# include "mpi.h"
# include <stdlib.h>
# include <sstream>
# include <string>
# include <iterator>
# include "backtracking.h"
# include <stack>

# define SONDAJ 0
# define ECOU 1
# define ME 42
# define START_ROUTING 100
# define STOP_ROUTING -100
# define ROUTER 666
# define FLAGS 2
# define ROOT 0
# define OCUPAT 100
# define LIBER -100

void rezolvSudoku ( char *inFile , char *outFile, std :: vector < std :: pair < int, int> > &kids,
		int &rank, int &parinte, int &tag, MPI_Status &Stat, int &numThreads, int &square );

void impartPatratele ( std :: vector < std :: pair < int, int> > &kids, int &parinte, int &rank, 
		int *rutare, int &numThreads, int &square, int &tag , MPI_Status &Stat );

void determinTopologie (char *nume , int &rank , int *initialMatrix, int &numThreads, int *recv,
		int *partial, int &sons, int &parinte, MPI_Status &Stat, int &tag, int &dimensiune, 
		int *rutare );

void startHomework (int argc, char *argv[]);

void waitSolutionsFromSons ( Backtracking &bt, int &square, std :: vector < std :: pair <int,int> > &kids,
		std :: vector < std :: pair <int, std :: vector < std :: vector<int> > > > *allSolutions,
			MPI_Status &Stat, int *sq, int &numThreads, int *matrix, int &sudoku, char *outFile,
			int &flags, int &parinte, int &rank );

void putSolutionInVector ( std :: vector <int> &sol, int &square,
		std :: vector < std :: pair <int, std :: vector < std :: vector<int> > > > *allSolutions);

void waitForKidsSolutions ( std :: vector < std :: pair <int,int> > &kids, MPI_Status &Stat ,
		std :: vector < std :: pair <int, std :: vector < std :: vector<int> > > > *allSolutions,
			int *sq, int &numThreads, int &square );

void putMySolutions ( Backtracking &bt, int &square,
	std :: vector < std :: pair <int, std :: vector < std :: vector<int> > > > * allSolutions );

void nodeSendsSolutionsToFather (Keep &ks, int &flags, int &parinte, int &rank, int &numThreads,
			int *sq );

void leafSendsSolutionsToFather ( int &flags , Backtracking &bt, int &parinte, int &rank,
			 int &numThreads, int *sq, int &square );

void recvSquare ( int *asigned , int &numThreads , int &parinte , int &tag , MPI_Status &Stat,
					int &square );

void splitConfiguration ( int &square , int *asigned , int &k, int &numThreads , int &tag,
				 std :: vector < std :: pair <int,int> > & kids);

void vectorToArray ( int &size , int *array , std :: vector <int> &vector, int &square );

void arrayToVector ( int &size , int *array , std :: vector <int> & vector, int &square );

void startSudoku ( std :: vector < std :: pair <int,int> > &kids , int &rank, int & parinte ,
						Backtracking &bt , int &dim, int &tag , MPI_Status &Stat ,int *matrix ,
						int &sudoku , int &numThreads, int &square , char *outFile) ;

void splitTheSquares ( int & rank , std :: vector < std :: pair <int,int> > & kids, int &numThreads,
						int & square , int & parinte, int &tag, MPI_Status &Stat );

void getChildrenSons ( std :: vector < std :: pair < int ,int > > & kids , int & parinte , int &rank,
						int *rutare , int &numThreads );

void readMatrix ( char *sudokuIn , int &n , int &dim , int *& matrix );

void put_routing_table ( int &rank , int *rutare , int &numThreads , int *matrix );

void set_routing_table ( int *rutare , int *initialMatrix , int &numThreads, int &dimensiune );

void initialized_routing_table ( int &numThreads , int &dimensiune , int *initialMatrix );

void wait_4_sondaj ( int & parinte , int *recv , int &numThreads, int &tag,  MPI_Status &Stat ,
						int &dimensiune  );

void send_to_parent ( int &numThreads , int *initialMatrix , int &parinte , int &dimensiune );

void read_configuration ( char *nume , int &rank , int *initialMatrix , int &dim );

std :: vector < std :: string > toArray ( std :: string &line );

void combine_results ( int &dimensiune , int *recv , int * initialMatrix );

void do_routing ( int &son , int &rank , int &numThreads , int * recv , int *initialMatrix ,
					int &dimensiune);

void wait_for_reply ( int & sons , int *recv , int *partial , int * initialMatrix , int &numThreads ,
						int &rank , MPI_Status &Stat, int &tag, int &dimensiune  );

void check_message_recv ( int * recv , int *initialMatrix , int &numThreads , int &son , int &rank , 
							int * partial , int & sons, int &dimensiune , int &tag);

void cut_the_branch ( int &rank , int & sons , int * initialMatrix , int * partial , int &son , 
						int &numThreads , int &dimensiune , int &tag );

void send_sondaj_to_all_sons ( int &sons , int &parinte , int &numThreads , int * initialMatrix ,
								int *partial , int &rank , int &tag, int &dimensiune );

void root_job ( int *initialMatrix, int *recv , int * partial, int &numThreads, int &sons,
					int &parinte , int &rank , MPI_Status &Stat , int &tag , int & dimensiune );
				
void send_ecou ( int *initialMatrix , int &dimensiune , int &numThreads , int &parinte , int &tag , 
					int type); 

void rest_of_the_world ( int &parinte , int *recv , int &numThreads , int &tag , MPI_Status &Stat ,
						int &dimensiune, int &sons, int *initialMatrix, int *partial, int &rank );
							
void alloc_and_initialize ( int &dimensiune , int &numThreads , int *&initialMatrix , int *&partial,
							int *&recv , int *&rutare , int & parinte );


void free_memory ( int *& recv , int *&rutare , int *&partial , int *&initialMatrix );

void write_matrix_to_file ( std :: ofstream &fp , int *matrix, int &n );


# endif
