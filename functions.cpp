/*
 * 
 * Codrut Cristian Grosu
 * codrut.cristian.grosu@gmail.com
 * Anul III - Seria CA - Grupa 333
 * Tema 3 - APD
 * 
 */

# ifndef __FUNCTIONS_CPP_
# define __FUNCTIONS_CPP_

# include "main.h"

/*
 * Functia incepe efectiv rezolvarea jocului sudoku.
 */
void rezolvSudoku ( char *inFile , char *outFile, std :: vector < std :: pair < int, int> > &kids,
		int &rank, int &parinte, int &tag, MPI_Status &Stat, int &numThreads , int &square ) {

	int n, dim, *matrix;
	// fiecare proces citeste matricea de la sudoku
	readMatrix (  inFile , n , dim , matrix );

	// fiecare proces is calculeaza solutia pentru patratul sau
	Backtracking bt ( n , matrix , square );
	bt.start ();
	
	// fiecare proces isi actualizeaza intreaga matrice dupa ce se aplica propagarea constrangerilor
	bt.updateMatrix(matrix);
	
	// procesle incep sa trimita solutiile lor, iar procesele care primesc solutiile le proceseaza
	startSudoku ( kids , rank, parinte ,bt , dim, tag , Stat  ,matrix , n , numThreads ,
						square , outFile );

	delete [] matrix;
}

/*
 * Impart cate un patrat la fiecare nod.
 */
void impartPatratele ( std :: vector < std :: pair < int, int> > &kids, int &parinte, int &rank, 
		int *rutare, int &numThreads, int &square, int &tag , MPI_Status &Stat ) {

	// Aflu cati copii am pe fiecare ramura
	getChildrenSons ( kids , parinte , rank,rutare , numThreads );
	
	// Fiecare proces va stii ce patrat trebuie sa rezolve
	splitTheSquares ( rank , kids, numThreads, square ,parinte, tag, Stat )	;

}

/*
 * Functia determina topologia retelei si tabela de rutare a fiecarui nod.
 */
void determinTopologie (char *nume , int &rank , int *initialMatrix, int &numThreads, int *recv,
		int *partial, int &sons, int &parinte, MPI_Status &Stat, int &tag, int &dimensiune, 
		int *rutare ) {

	// fiecare nod isi citeste si pastreaza configuratia
	read_configuration ( nume , rank , initialMatrix , numThreads );

	if ( ROOT == rank ) {
	
		// initiatorul porneste algoritmul sondaj-unda
		root_job ( initialMatrix, recv , partial, numThreads, sons, parinte , rank , Stat ,
					 tag , dimensiune );

	} else  {
	
		// restul proceselor participa si ele la algoritmul sondaj-unda
		rest_of_the_world ( parinte , recv , numThreads , tag , Stat , dimensiune, sons,
				 initialMatrix, partial, rank );
		
	}

	// vectorul rutare va contine tabela de rutare
	set_routing_table ( rutare , initialMatrix , numThreads , dimensiune );
	
	// scriu rezultatele fiecarui proces intr-un fisier separat
	put_routing_table ( rank , rutare , numThreads , initialMatrix ); 

}

/*
 * Start tema.
 */
void startHomework (int argc, char *argv[]) {

	int dimensiune, *initialMatrix, *partial, *recv, *rutare;
	int rank, numThreads, square, tag = 1, sons, parinte;
	std :: vector < std :: pair < int, int> > kids;
	MPI_Status Stat;
		
    MPI_Init(&argc,&argv);
    MPI_Comm_size(MPI_COMM_WORLD, &numThreads);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

	// aloc memorie si initializez datele pentru fiecare proces
	alloc_and_initialize ( dimensiune , numThreads , initialMatrix , partial, recv ,
					 rutare , parinte );

	// determin topologia si tabela de rutare
	determinTopologie ( argv[1] , rank ,  initialMatrix,  numThreads,  recv, partial, sons, parinte,
						Stat, tag, dimensiune, rutare );

	// fiecare proces va avea un unic patrat de rezolvat
	impartPatratele ( kids, parinte, rank, rutare, numThreads, square, tag , Stat );
	
	// aici incepe efectiv rezolvarea jocului sudoku
	rezolvSudoku ( argv[2] , argv[3] , kids, rank, parinte, tag, Stat, numThreads , square );
	
	// eliberez memoria alocata pentru fiecare proces :
	free_memory ( recv , rutare , partial , initialMatrix );

	MPI_Finalize();

}


/*
 * Nodul care nu este frunza si nici root trimite toate solutiile pe care le-a pastrat parintelui sau.
 */
void nodeSendsSolutionsToFather (Keep &ks, int &flags, int &parinte, int &rank, int &numThreads,
			int *sq ) {

	std :: vector < std :: pair <int, std :: vector < std :: vector<int> > > > *send;
	send = ks.getSend ();
	flags = ks.totalSquare();
	MPI_Send ( &flags , 1 , MPI_INT , parinte , rank , MPI_COMM_WORLD );	
	for ( unsigned int j = 0 ; j < send->size () ; j ++ ) {
			
		for ( unsigned int i = 0 ; i < send->at(j).second.size () ; i ++ ) {
				
			vectorToArray ( numThreads , sq , send->at(j).second.at(i) , send->at(j).first );
			MPI_Send ( sq , numThreads + 1 , MPI_INT , parinte , rank , MPI_COMM_WORLD );
				
		}
			
	}
	
	delete send;

}

/*
 * Nodul frunza trimite toate solutiile sale parintelui sau.
 */
void leafSendsSolutionsToFather ( int &flags , Backtracking &bt, int &parinte, int &rank,
			 int &numThreads, int *sq, int &square ) {

	flags = bt.numarSolutii ();
	MPI_Send ( &flags , 1 , MPI_INT , parinte , rank , MPI_COMM_WORLD );
	for (unsigned int i = 0 ; i < bt.getAllSolutions ().size () ; i ++ ) {
		
		vectorToArray ( numThreads , sq , bt.getAllSolutions ()[i] , square) ;
		MPI_Send ( sq , numThreads + 1 , MPI_INT , parinte, rank , MPI_COMM_WORLD );
		
	}

}

/*
 * Functia pune toate solutiile nodului curent in vectorul allSolutions.
 */
void putMySolutions ( Backtracking &bt, int &square,
	std :: vector < std :: pair <int, std :: vector < std :: vector<int> > > > * allSolutions ) {
	
	std :: pair <int, std :: vector < std :: vector<int> > > eu;
	eu.first = square;
	eu.second = bt.getAllSolutions ();
	allSolutions->push_back (eu);
	bt.clear ();

}


/*
 * Functia pune o solutie in vectorul allSolutions.
 */
void putSolutionInVector ( std :: vector <int> &sol, int &square,
		std :: vector < std :: pair <int, std :: vector < std :: vector<int> > > > *allSolutions) {

	std :: vector < std :: pair <int, std :: vector < std :: vector<int> > > >  :: iterator it;
	int ok = 1;
	for ( it = allSolutions->begin () ; it != allSolutions->end () ; ++ it ) {
				
		if ( it->first == square ) {
					
			it->second.push_back ( sol );
			ok = 0;
			break;
					
		}
		
	}
	if (ok) {
				
		std :: pair < int , std :: vector < std :: vector < int > > > p;
		p.first = square;
		p.second.push_back (sol);
		allSolutions->push_back ( p );
				
	}

}

/*
 * Nodul curent asteapta toate solutiile de la fii sai.
 */
void waitForKidsSolutions ( std :: vector < std :: pair <int,int> > &kids, MPI_Status &Stat ,
		std :: vector < std :: pair <int, std :: vector < std :: vector<int> > > > *allSolutions,
			int *sq, int &numThreads, int &square ) {

	int solutii, son;
	for ( unsigned int i = 0 ; i < kids.size () ; i ++ ) {
		
		MPI_Recv ( &solutii , 1 , MPI_INT , MPI_ANY_SOURCE , MPI_ANY_TAG , MPI_COMM_WORLD, &Stat);
		son = Stat.MPI_TAG;
		for ( int i = 0; i < solutii ; i ++ ) {
			
			MPI_Recv ( sq , numThreads + 1 , MPI_INT , son , son ,  MPI_COMM_WORLD, &Stat);
			std :: vector <int> sol;
			arrayToVector ( numThreads , sq , sol , square );
			
			// pun solutia primita in vectorul allSolutions
			putSolutionInVector ( sol, square, allSolutions );
			
		}
			
	}

}

/*
 * Nodul curent (care nu este frunza) asteapta toate solutiile de la copii sai. Le elimina pe cele
 * care nu sunt bune. Daca este root, scrie solutia in fisierul de iesire altfel trimite solutiile
 * ramase la parintele sau.
 */
void waitSolutionsFromSons
		 ( Backtracking &bt, int &square, std :: vector < std :: pair <int,int> > &kids,
		std :: vector < std :: pair <int, std :: vector < std :: vector<int> > > > *allSolutions,
			MPI_Status &Stat, int *sq, int &numThreads, int *matrix, int &sudoku, char *outFile,
			int &flags, int &parinte, int &rank ) {

	// pun toate solutiile mele in vectorul allSolutions
	putMySolutions (bt, square, allSolutions );
		
	// Astept toate solutiile de la fii mei si le pun in vectorul allSolutions
	waitForKidsSolutions ( kids, Stat ,allSolutions, sq, numThreads, square );

	Keep ks ( allSolutions,  matrix , sudoku );
	if ( rank == ROOT ) {
		
		ks.setRoot ();
		ks.setWrite (outFile);
		ks.endHomework ();
		
	} else {
		
		// nodul isi trimite solutiile tatalui sau
		nodeSendsSolutionsToFather ( ks, flags, parinte, rank, numThreads, sq );
	
	}

}

/*
 * Functia se ocupa cu rezolvarea jocului sudoku.
 */
void startSudoku ( std :: vector < std :: pair <int,int> > &kids , int &rank, int & parinte ,
						Backtracking &bt , int &dim, int &tag , MPI_Status &Stat ,int *matrix ,
						int &sudoku , int &numThreads, int &square , char *outFile ) {

	int flags;
	int *sq = new int [numThreads+1];
	std :: vector < std :: pair <int, std :: vector < std :: vector<int> > > > *allSolutions;
	std :: vector < std :: pair <int, std :: vector < std :: vector<int> > > >  :: iterator it;
	allSolutions = new std :: vector < std :: pair <int, std :: vector < std :: vector<int> > > >();
	if ( kids.size () == 0 ) {
	
		// frunza isi trimite solutiile pentru patrat la parinte
		leafSendsSolutionsToFather ( flags , bt, parinte, rank, numThreads, sq, square );
	
	} else {
	
		// restul nodurilor asteapta solutiile de la fii si le proceseaza
		waitSolutionsFromSons ( bt, square, kids, allSolutions, Stat, sq, numThreads, matrix, 
			sudoku, outFile, flags, parinte, rank );
		
	}
	
	delete [] sq;
	delete allSolutions;

}

/*
 * Functia se ocupa cu impartirea corespunzatoare a patratelor pentru fiecare fiu pe care nodul 
 * curent il are.
 */
void splitConfiguration ( int &square , int *asigned , int &k, int &numThreads , int &tag,
				 std :: vector < std :: pair <int,int> > & kids ) {

	std :: vector < std :: pair <int,int> > :: iterator it;
	k = square + 1;
	for ( it = kids.begin () ; it != kids.end () ; ++it ) {
	
		memset ( asigned , OCUPAT , sizeof (int) * numThreads );
		if ( it->second == 1 ) {
			
			// daca fiul este frunza, ii trimit doar un patrat
			asigned[k] = LIBER;
			k++;
			
		} else {
			
			// daca fiul acest nu este frunza, ii trimit un patrat pentru fiecare fiu
			for ( int i = 0 ; i < it->second ; i ++ ) {
				
				asigned[k] = LIBER;
				k ++;
				
			}
			
		}
		MPI_Send ( asigned , numThreads , MPI_INT , it->first , tag , MPI_COMM_WORLD);
		
	}

}

/*
 * Functia primeste un mesaj in care se afla patratul pe care trebuie sa il rezolve.
 */
void recvSquare ( int *asigned , int &numThreads , int &parinte , int &tag , MPI_Status &Stat,
					int &square ) {

	MPI_Recv ( asigned , numThreads  , MPI_INT , parinte , tag , MPI_COMM_WORLD , &Stat );
	for ( int i = 0 ; i < numThreads; i ++ ) {
			
		if ( asigned[i] == LIBER ) {
				
			square = i;
			break;
				
		}
			
	}

}

/*
 * Functia se ocupa cu impartirea patratelor de la sudoku care trebuie rezolvate.
 */
void splitTheSquares ( int & rank , std :: vector < std :: pair <int,int> > & kids, int &numThreads,
						int & square , int & parinte, int &tag, MPI_Status &Stat ) {

	int *asigned = new int [ numThreads ];
	std :: vector < std :: pair <int,int> > :: iterator it;
	int k;
	if ( rank == ROOT ) {
	
		// root-ul trimite primul configuratia si isi pastreaza primul patrat
		square = ROOT;
		asigned[square] = OCUPAT;
		splitConfiguration ( square , asigned , k, numThreads , tag, kids);
	
	} else {
	
		// identific patratul pe care il voi rezolva
		recvSquare ( asigned , numThreads , parinte , tag , Stat,square );
		if ( kids.size () != 0 ) {
		
			// daca mai am vecini le trimit restul patratelor nealocate
			splitConfiguration ( square , asigned , k, numThreads , tag, kids );
			
		}
	
	}
	delete [] asigned;

}

/*
 * Functia copiaza elementele unui int* in std :: vector si extrage numarul patratului pe care l-a
 * copiat.
 */
void arrayToVector ( int &size , int *array , std :: vector <int> & vector, int &square ) {

	for ( int i = 0 ; i < size ; i ++ ) {
	
		vector.push_back ( array[i] );
	
	}
	square = array[size];

}

/*
 * Functia copiaza elementele unui std :: vector in int* si pe ultima pozitie pune numarul 
 * patratului pe care l-a copiat.
 */
void vectorToArray ( int &size , int *array , std :: vector <int> &vector , int & square ) {

	for ( int i = 0 ; i < size ; i ++ ) {
	
		array[i] = vector[i];
	
	}
	array[size] = square;

}

/*
 *  Functia determina cati copii are fiecare fiu al nodului curent.
 */
void getChildrenSons ( std :: vector < std :: pair < int ,int > > & kids , int & parinte , int &rank,
						int *rutare , int &numThreads ) {

	int ok;
	for ( int i = 0 ; i < numThreads; i ++ ) {
	
		if ( rutare[i] != parinte && i != rank ) {
			ok = 1;
			for ( unsigned int j = 0 ; j < kids.size () ; j ++ ) {
				if ( kids[j].first == rutare[i] ) {
				
					ok = 0;
					kids[j].second ++;
					break;
					
				}
			
			}
			if ( ok ) {
			
				std :: pair <int,int> p;
				p.first = rutare[i];
				p.second = 1;
				kids.push_back (p);
			
			}
		
		}
	
	}
	
}

/*
 * Functia citeste configuratia pentru jocul de sudoku.
 */
void readMatrix ( char *sudokuIn , int &n , int &dim , int *& matrix ) {

	std :: ifstream f ( sudokuIn );
	f >> n;
	dim = n*n;
	matrix = new int [ dim * dim ];
	for ( int i = 0 ; i < dim * dim ; i ++ ) {
			
		f >> matrix[i];
			
	}
	f.close ();

}

/*
 * Functia initializeaza tabela de rutare.
 */
void initialized_routing_table ( int &numThreads , int &dimensiune , int *initialMatrix ) {

	for ( int i = 0 ; i < numThreads ; i ++ ) {
	
		initialMatrix [ dimensiune + FLAGS + i ] = ROUTER;
	
	}

}

/*
 * Functia trimite ecou la parinte in functie de type.
 */
void send_ecou ( int *initialMatrix , int &dimensiune , int &numThreads , int &parinte ,
							int &tag , int type ) {

	initialMatrix[0] = ECOU;
	initialMatrix[1] = type;
	MPI_Send ( initialMatrix , dimensiune + FLAGS + numThreads  , MPI_INT , parinte ,
		 tag, MPI_COMM_WORLD);

}

/*
 * Fiecare proces care nu este root asteapta sondaj de la parintele sau.
 */
void wait_4_sondaj ( int & parinte , int *recv , int & numThreads , int &tag,  MPI_Status &Stat ,
						int & dimensiune ) {

	// Astept sondaj de la parintele meu
	memset ( recv , 0 , ( dimensiune + FLAGS + numThreads ) * sizeof( int ));
	MPI_Recv ( recv , dimensiune + FLAGS + numThreads , MPI_INT , MPI_ANY_SOURCE , tag,
				 MPI_COMM_WORLD, &Stat);
	parinte = Stat.MPI_SOURCE;

}

/*
 * Functia completeaza mesajul care va fi trimis la parinte cu tabela de rutare a nodului curent.
 */
void send_to_parent ( int &numThreads , int *initialMatrix, int &parinte , int &dimensiune ) {

	// daca sunt frunza => completez corespunzator tabela de rutare
	for ( int i = 0 ; i < numThreads ; i ++ ) {
			
		if ( initialMatrix[dimensiune + FLAGS + i ] == ROUTER ) {
		
			initialMatrix[dimensiune + FLAGS + i ] = parinte;
				
		}
			
	}

}

/*
 * Functia se ocupa de task-urile pe care trebuie sa le indeplineasca initiatorul algoritmului
 * pentru determinarea topologiei retelei.
 */
void root_job ( int *initialMatrix, int *recv , int * partial, int &numThreads, int &sons,
				int &parinte , int &rank , MPI_Status &Stat , int &tag , int &dimensiune ) {

	// trimite sondaj fiilor sai
	send_sondaj_to_all_sons ( sons , parinte , numThreads , initialMatrix , partial ,
				 rank , tag , dimensiune );

	// Astept reply de la fii mei
	wait_for_reply ( sons , recv , partial , initialMatrix , numThreads , rank, Stat ,
				 tag , dimensiune );

}

/*
 * Fiecare proces isi scrie tabela de rutare intr-un fisier separat.
 * Procesul cu id-ul 0 (care este si root) scrie intreaga topologie.
 */
void put_routing_table ( int &rank , int *rutare , int &n, int *matrix ) {

	char a[] = "routing";
	char numar[10];
	sprintf ( numar, "%d", rank );
	char *nume = strcat ( a , numar );
	std :: ofstream fp ( nume );
	
	for ( int i = 0 ; i < n ; i ++ ) {
		
		if ( i != rank ) {
		
			fp << "Sursa : " << rank << " --- Destinatie : " << i;
			fp << " --- Via : " << rutare[i] << "\n"; 
			
		}
	
	}
	
	if ( rank == 0 ) {
	
		write_matrix_to_file ( fp , matrix, n );
	
	}
	fp.close ();
	
}


/*
 * Functia citeste lista de adiacenta a unui proces si stocheaza datele intr-o matrice.
 */
void read_configuration ( char *nume , int &rank , int *initialMatrix, int &dim ) {

	std :: ifstream f ( nume );
	std::string line;
	int i = 0;
	
	while ( std::getline(f, line) ) {
	
		// daca am citit linia corespunzatoare procesului curent
		if ( i == rank ) {
		
			std :: vector < std :: string > vstrings = toArray ( line );
			for ( unsigned int j = 2 ; j < vstrings.size () ; j ++ ) {

				int nr = atoi(vstrings[j].c_str());
				initialMatrix[FLAGS + dim * rank + nr ] = 1;
			
			}
			break;
		
		} else {
		
			i ++;
			
		}
	
	}
	
	f.close ();

}

/*
 * Functia transforma un string intr-un array de stringuri in functie de space.
 */
std :: vector < std :: string > toArray ( std :: string &line ) {

	std::stringstream ss(line);
	std::istream_iterator<std::string> begin(ss);
	std::istream_iterator<std::string> end;
	std::vector<std::string> vstrings (begin, end);
	
	return vstrings;
			
}

/*
 * Functia trimite sondaj la toti fii mei.
 */
void send_sondaj_to_all_sons ( int &sons , int &parinte , int &numThreads , int * initialMatrix ,
								int *partial , int &rank , int &tag, int &dimensiune  ) {

	sons = 0;
	for ( int j = 0 ; j < numThreads ; j ++ ) {
		
		if ( initialMatrix[ FLAGS + j + numThreads*rank ] == 1 && j != parinte ) {
			
			sons ++;
			memset ( partial , 0 , ( dimensiune + FLAGS + numThreads ) * sizeof (int) );
			partial[0] = SONDAJ;
			partial[1] = STOP_ROUTING;
			MPI_Send ( partial , dimensiune + FLAGS + numThreads , MPI_INT , j ,
					 tag , MPI_COMM_WORLD);
			
		}
		
	}

}

/*
 * Functia executa SAU intre elementele binare ale doua matrici.
 */
void combine_results ( int &dimensiune , int *recv , int * initialMatrix ) {

	for ( int i = FLAGS ; i <= dimensiune ; i ++ ) {
				
		if ( recv[i] == 1 || initialMatrix[i] == 1 ) {
					
			initialMatrix[i] = 1;
					
		}
				
	}

}

/*
 * Functia se ocupa cu configurarea tabelei de rutare.
 */
void do_routing ( int &son, int &rank , int &numThreads , int * recv , int *initialMatrix ,
					int &dimensiune ) {

	if ( recv[1] == START_ROUTING ) {
				
		for ( int i = 0 ; i < numThreads ; i ++ ) {
			
			// Daca i este vecin cu mine => trec prin el ca sa ajung la el
			if ( initialMatrix[ FLAGS + numThreads * rank + i ] == 1 ) {
							
				//in tabela de rutare pun la rut[i] = i;
				initialMatrix[dimensiune + FLAGS + i ] = i;
							
			} else if ( i == rank ) {
							
				// in tabela mea de rutare, eu sunt omis
				initialMatrix[ dimensiune + FLAGS + rank ] = ME;
							
			} else if ( recv[ dimensiune + FLAGS + i ] != rank ) {
							
				// daca pentru nodul curent fiul meu stie pe unde sa mearga =>
				// eu trec pe la el
				initialMatrix[ dimensiune + FLAGS + i ] = son;
							
			}
			
		}
				
	}

}

/*
 * Functia verifica ce tip de mesaj s-a primit si procedeaza in functie de tipul mesajului.
 */
void check_message_recv ( int * recv , int *initialMatrix , int &numThreads , int &son , int &rank , 
							int * partial , int & sons , int &dimensiune , int &tag ) {

	if ( recv[0] == ECOU ) {
				
		// daca am primit ecou => combin rezultatele
		combine_results ( dimensiune , recv , initialMatrix );
		// daca trebuie sa rutez => construiesc tabela mea de rutare
		do_routing ( son , rank , numThreads , recv , initialMatrix , dimensiune );
				
	}
	 else if ( recv[0] == SONDAJ ) {
				  
		// daca am primit de la un fiu un mesaj de tip sondaj => tai ramura
		cut_the_branch ( rank , sons , initialMatrix , partial , son ,
					 numThreads , dimensiune , tag );
				
	}

}

/*
 * Functia asteapa ca toti fii sa raspunda.
 */
void wait_for_reply ( int & sons , int *recv , int *partial , int * initialMatrix , int &numThreads ,
						int &rank , MPI_Status & Stat , int &tag , int &dimensiune ) {

	int son;
	
	for ( int j = 0 ; j < sons ; j ++ ) {
			
		memset ( recv , 0 , (dimensiune + FLAGS + numThreads ) * sizeof(int) );
		MPI_Recv ( recv , dimensiune + FLAGS + numThreads , MPI_INT , MPI_ANY_SOURCE ,
					 tag, MPI_COMM_WORLD, &Stat);
		son = Stat.MPI_SOURCE;

		// verific ce mesaj am primit si procedez ca atare.
		check_message_recv ( recv , initialMatrix , numThreads , son , rank , partial ,
				 sons , dimensiune, tag );
			
	}

}

/*
 * Functia se ocupa de eliminarea acelor muchii care formeaza cicluri.
 */
void cut_the_branch ( int &rank , int & sons , int * initialMatrix , int * partial , int &son , 
						int &numThreads , int &dimensiune , int &tag ) {

	sons ++;
	initialMatrix[ FLAGS + son + numThreads*rank ] = 0;
	memset ( partial , 0 , ( dimensiune + FLAGS + numThreads ) * sizeof (int) );
	send_ecou ( partial  , dimensiune , numThreads , son ,tag , STOP_ROUTING );
	
}

/*
 * Functia se ocupa de task-urile pe care trebuie sa le fac nodurile care nu sunt root la 
 * algoritmul de determinare a topologiei retelei.
 */
void rest_of_the_world ( int &parinte , int *recv , int &numThreads , int &tag , MPI_Status &Stat ,
						int &dimensiune, int &sons, int *initialMatrix, int *partial, int &rank ) {

	// astept sondaj de la parintele meu
	wait_4_sondaj ( parinte , recv , numThreads , tag , Stat , dimensiune );
		
	// Trimit Sondaj la toti fii mei
	send_sondaj_to_all_sons ( sons , parinte , numThreads , initialMatrix , partial ,
				 rank , tag , dimensiune );	

	if ( sons != 0 ) {
		
		// Daca nu sunt frunza astept mesaj de la fii mei
		wait_for_reply ( sons , recv , partial , initialMatrix , numThreads , rank , Stat ,
					 tag , dimensiune );
			
		// completez restul tabelei de rutare
		send_to_parent ( numThreads , initialMatrix , parinte , dimensiune );
		
	} else {
		
		// Daca sunt frunza, completez tabela de rutare corespunzator
		send_to_parent ( numThreads , initialMatrix, parinte , dimensiune );
		initialMatrix[dimensiune + FLAGS + rank] = ME;
		
	}
		
	// Trimit ecou la parinte
	send_ecou ( initialMatrix , dimensiune , numThreads , parinte ,tag , START_ROUTING );

}

/*
 * Functia initializeaza tabela de rutare care va fi trimisa parintelui.
 */
void set_routing_table ( int *rutare , int *initialMatrix , int &numThreads , int &dimensiune ) {

	for ( int i = 0 ; i < numThreads ; i ++ ) {
	
		rutare[i] = initialMatrix [ FLAGS + dimensiune + i ];
	
	}

}

/*
 * Functia aloca memorie si initializeaza variabilele.
 */
void alloc_and_initialize ( int &dimensiune , int &numThreads , int *&initialMatrix , int *&partial,
							int *&recv , int *&rutare , int & parinte ) {

	parinte = -1;
	dimensiune = numThreads*numThreads;
	initialMatrix = new int [ dimensiune + FLAGS + numThreads ];
	partial = new int [ dimensiune + FLAGS + numThreads ] ;
	recv = new int [ dimensiune + FLAGS + numThreads ];
	rutare = new int [numThreads];
	memset ( initialMatrix , 0 , ( dimensiune + FLAGS  ) * sizeof (int) );
	initialized_routing_table ( numThreads , dimensiune ,initialMatrix );

}

/*
 * Functia elibereaza memoria alocata.
 */
void free_memory ( int *& recv , int *&rutare , int *&partial , int *&initialMatrix ) {

	delete [] initialMatrix;
	delete [] recv;
	delete [] partial;
	delete [] rutare;


}

/*
 * Functia scrie o matrice intr-un fisier.
 */
void write_matrix_to_file ( std :: ofstream &fp , int *matrix, int &n ) {

	fp << "\nMatricia de adiacenta a topologiei :\n\n";
	for ( int i = 0 ; i < n ; i ++ ) {
	
			for ( int j = 0 ; j < n ; j ++ ) {
		
				fp << matrix[ FLAGS + i * n + j ] << " ";
		
			}
		
			fp << "\n";
	
		}

}


# endif
