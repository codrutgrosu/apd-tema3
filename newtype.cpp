
/*# include <iostream>
# include "mpi.h"

struct Tip {

	int flags[3];
	int dimensiune;
	int matrix[16];
	

};

int main (int argc, char *argv[]) {

	int rank, tag=1;
	MPI_Status Stat;
	struct Tip mesaj;
	
	MPI_Datatype Codrut;
	MPI_Datatype type[3] = { MPI_INT, MPI_INT , MPI_INT };
	int blocklen[3] = {3,1,16};
	MPI_Aint disp[3];
	
	disp[0] = 0;
	disp[1] = 12;
	disp[2] = 16;
		
	MPI_Init( &argc, &argv );

	MPI_Type_create_struct ( 3 , blocklen , disp , type , &Codrut );
	MPI_Type_commit (&Codrut);
	
	
	MPI_Comm_rank ( MPI_COMM_WORLD, &rank );
	
	if ( rank == 0 ) {
	
		mesaj.flags[0] = 28;
		mesaj.dimensiune = 3;
		mesaj.matrix[10] = 22;
		MPI_Send ( &mesaj , 1 , Codrut , 1 , tag , MPI_COMM_WORLD);
	
	} else {
	
		MPI_Recv ( &mesaj , 1 , Codrut , 0 , tag , MPI_COMM_WORLD, &Stat );
		
		std :: cout << "Rank : " << rank << "  mesaj.flags[0] = " << mesaj.flags[0] << "\n";
		std :: cout << "Rank : " << rank << "  mesaj.dimensiune = " << mesaj.dimensiune << "\n";
		std :: cout << "Rank : " << rank << "  mesaj.matrix[10] = " << mesaj.matrix[10] << "\n";
	
	}
	
	MPI_Finalize();
    return 0;

}*/

#include "mpi.h"
#include <iostream>

int main(int argc, char *argv[])  {
int numtasks, rank, next, prev, buf[2], tag1=1, tag2=2;
MPI_Request reqs[4];
MPI_Status stats[4];

MPI_Init(&argc,&argv);
MPI_Comm_size(MPI_COMM_WORLD, &numtasks);
MPI_Comm_rank(MPI_COMM_WORLD, &rank);

prev = rank-1;
next = rank+1;
if (rank == 0)  prev = numtasks - 1;
if (rank == (numtasks - 1))  next = 0;

memset ( buf, 0 , 2 * sizeof (int) );
MPI_Irecv(&buf[0], 1, MPI_INT, prev, tag1, MPI_COMM_WORLD, &reqs[0]);
MPI_Irecv(&buf[1], 1, MPI_INT, next, tag2, MPI_COMM_WORLD, &reqs[1]);

  //std :: cout << "Rank : " << rank << " --- Recv_prev : " << buf[0] << " --- Recv_next : " << buf[1] << "\n";


MPI_Isend(&rank, 1, MPI_INT, prev, tag2, MPI_COMM_WORLD, &reqs[2]);
MPI_Isend(&rank, 1, MPI_INT, next, tag1, MPI_COMM_WORLD, &reqs[3]);

MPI_Waitall(4, reqs, stats);
 std :: cout << "Rank : " << rank << " --- Recv_prev : " << buf[0] << " --- Recv_next : " << buf[1] << "\n";

MPI_Finalize();
return 0;
}
