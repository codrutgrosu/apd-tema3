/*
 * 
 * Codrut Cristian Grosu
 * codrut.cristian.grosu@gmail.com
 * Anul III - Seria CA - Grupa 333
 * Tema 3 - APD
 * 
 */
# ifndef __MAIN_CPP_
# define __MAIN_CPP_

# include "main.h"

int main (int argc, char *argv[]) {

	if ( argc != 4 ) {
	
		std :: cout << "Error --- numar insuficient de argumente in linie de comanda.\n";
	
	} else {
	
		startHomework (argc,argv);
	
	}
	return 0;

}
# endif
