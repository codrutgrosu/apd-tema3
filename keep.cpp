/*
 * 
 * Codrut Cristian Grosu
 * codrut.cristian.grosu@gmail.com
 * Anul III - Seria CA - Grupa 333
 * Tema 3 - APD
 * 
 */
# ifndef __KEEP_CPP_
# define __KEEP_CPP_

# include "keep.h"

/*
 * Functia realizeaza punerea unui patrat in matricea de sudoku in pozitia corespunzatoare patratului.
 */
void Keep :: putSquareInMatrix ( int &square , std :: vector <int> sol , int *matrix ,
			int &numThreads , int &n ) {
		 
	int start = ( square / n ) * n * numThreads + (square % n ) * n;
	for ( int i = 0 ; i < n ; i ++ ) {
	
		for ( int j = 0 ; j < n ; j ++ ) {
		
			matrix[start+i*numThreads +j] = sol[i*n+j];
		
		}
	
	}
	
}

/*
 * Functia realizeaza copierea unei matrici in alta matrice.
 */
void Keep :: copyMatrix ( int size , int * src , int * dest ) {

	for ( int i = 0 ; i < size ; i ++ ) {
	
		dest[i] = src[i];
	
	}

}

/*
 * Functia intoarce numaru de patrate care vor trebui trimise parintelui.
 */
int Keep :: totalSquare() {

	int suma = 0;
	for ( unsigned int i = 0 ; i < this->send->size () ; i ++ ) {
	
		suma += this->send->at(i).second.size ();
	
	}
	return suma;

}

/*
 * Se elibereaza o parte din vectori folositi.
 */
void Keep :: clear () {

	this->theEnd.clear ();
	this->keep.clear ();

}

/*
 * Se seteaza fisierul de output unde se va scrie prima solutie a jocului.
 */
void Keep :: setWrite ( char * out ) {

	this->write = out;

}

/*
 * Se marcheaza ca aceasta instanta apartine root-ului.
 */
void Keep :: setRoot () {

	this->root = true;

}


/*
 * Functia genereaza prima solutie si o scrie in fisierul de output.
 */
void Keep :: endHomework () {


	startGenerate ();
	std :: ofstream fp ( this->write );
	std :: vector < std :: pair <int, std :: vector <int> > > :: iterator it;
	
	copyMatrix ( this->dimensiune * this->dimensiune , this->matrix , this->test );
	for ( it = this->theEnd.begin () ; it != this->theEnd.end () ; ++it ) {
	
		putSquareInMatrix ( it->first , it->second , this->test , this->dimensiune , this->N );
	
	}
	fp << this->N << "\n";
	for ( int i = 0 ; i < this->dimensiune ; i ++ ) {
	
		for ( int j = 0 ; j < this->dimensiune - 1; j ++ ) {
		
			fp << this->test[i * this->dimensiune + j] << " ";
		
		}
		
		fp << this->test[i * this->dimensiune + this->dimensiune - 1] << "\n";
	
	}
	
	fp.close ();
	this->clear ();
	
}

/*
 * Functia genereaza toate configuratile pe care le poate genera cu patratele pe care le are si 
 * intoarce vectorul de patrate care au compus o solutie.
 */
std :: vector < std :: pair <int, std :: vector < std :: vector<int> > > > * Keep :: getSend () {

	startGenerate ();
	for ( unsigned int i = 0 ; i < this->keep.size () ; i ++ ) {
	
		for ( unsigned int j = 0 ; j < this->send->size () ; j ++ ) {
		
			if ( this->keep.at(i).first == this->send->at(j).first ) {
			
				for ( unsigned int p = 0 ; p < this->keep.at(i).second.size () ; p ++ ) {
				
					if ( this->keep.at(i).second[p] == 1 ) {
					
						this->send->at(j).second.push_back ( this->initial->at(i).second[p] );
					
					}
				
				}
			
			}
		
		}
	
	}
	this->clear ();
	return this->send;

}

/*
 * Functia testeaza daca am ajuns la o solutie.
 */
int Keep :: solutie () {

	int size = this->initial->size ();
	return size - 1 == k;
	

}

/*
 * Functia verifica daca patratul curent poate fi parte dintr-o solutie.
 */
int Keep :: valid () {

	copyMatrix ( this->dimensiune * this->dimensiune , this->matrix , this->test );

	for ( int i = 0 ; i <= k ; i ++  ) {
	
		putSquareInMatrix ( this->initial->at(i).first ,
							 this->initial->at(i).second[ this->array[i] ] ,
		this->test , this->dimensiune , this->N );
	
	}


	return checkSquareSudoku (this->initial->at(k).first) ;
}

/*
 * Functia intoarce true daca patratul square este compatibil cu precedentele patrate.
 */
bool Keep :: checkSquareSudoku ( int square ) {

	int start = ( square / this->N ) * this->dimensiune * this->N + (square % this->N) * this->N;
	int pos, val, line, col;
	for ( int i = 0 ; i < this->N ; i ++ ) {
	
		for ( int j = 0 ; j < this->N ; j ++ ) {
		
			pos = start + i * this->dimensiune + j;
			line = pos / this->dimensiune;
			col = pos % this->dimensiune;
			val = this->test[pos];
			
			for ( int l = 0 ; l < this->dimensiune ; l ++ ) {
			
				if ( pos != line * this->dimensiune + l && 
						val == this->test [ line * this->dimensiune + l ] ) {

					return false;
				
				}
				if ( pos != this->dimensiune * l + col && 
						val == this->test [ this->dimensiune * l + col ] ) {

					return false;
				
				}
			
			}
		
		}

	}
	
	return true;
	
}

/*
 * Functia ajutatoare pentru backtracking
 */
void Keep :: init () {

	this->array[k] = -1;

}

/*
 * Functia ajutatoare pentru backtracking
 */
int Keep :: succesor () {

	int size = this->initial->at(k).second.size ();
	if ( this->array[k] < size - 1 ) {
	
		this->array[k] ++;
		return 1;
	
	}
	
	return 0;

}

/*
 * Functia pastreaza acele patrate care au folosit la generarea unei solutii.
 */
void Keep :: continueGenerating () {

	std :: vector < std :: pair <int, std :: vector < std :: vector<int> > > > :: iterator it;
	it = this->initial->begin ();
	for ( int i = 0 ; i <= k ; i ++ , it ++ ) {
				
		for ( unsigned int j = 0 ; j < this->send->size () ; j ++ ) {
						
			if ( this->keep.at(j).first == it->first ) {
							
				this->keep.at(j).second[ this->array[i] ] = 1;
							
			}
						
		}
				
	}

}

/*
 * Functia salveaza solutia care trebuie scrisa in fisierul de iesire.
 */
void Keep :: stopGenerating () {

	std :: vector < std :: pair <int, std :: vector < std :: vector<int> > > > :: iterator it;
	it = this->initial->begin ();
	for ( int i = 0 ; i <= k ; i ++ , it ++ ) {
					
		std :: pair < int , std :: vector <int> > p;
		p.first = it->first;
		p.second = it->second[ this->array[i] ];
		this->theEnd.push_back (p);
				
	}

}

/*
 * Functia se ocupa de generarea tuturor combinatiilor de patrate care se afla in vectorul initial.
 */
void Keep :: startGenerate () {

	this->k = 0;
	this->init ();
	
	while ( k > -1 ) {
	
		this->_succesor_exist = 1;
		this->_is_valid = 0;

		while ( this->_succesor_exist == 1 && ! this->_is_valid ) {
		
			this->_succesor_exist = this->succesor ();
			if ( this->_succesor_exist ) {
			
				this->_is_valid = this->valid ();
			
			}
			
		}
			
		if ( this->_succesor_exist ) {
		
			if ( this->solutie () ) {
			
				if ( this-> root == true ) {
				
					// daca sunt root : am nevoie doar de o singura solutie, apoi ma opresc
					this->stopGenerating ();
					return ;
				
				} else {
			
					this->continueGenerating ();
					
				}
				
			} else {
			
				this->k ++;
				this->init ();
			
			}
		
		} else {
		
			this->k --;
		
		}
	
	}

}


/*
 * Constructorul clasei Keep : aloca memorie si initializeaza variabilele.
 */
Keep :: Keep ( std :: vector < std :: pair <int, std :: vector < std :: vector<int> > > >  * sol ,
		int *matrix , int &N) {

	this->root = false;
	this->initial = sol;
	this->N = N;
	this->dimensiune = N*N;
	if ( this->matrix == NULL ) {
	
		this->matrix = new int [ this->dimensiune * this->dimensiune ];
	
	}
	
	if ( this->array == NULL ) {
	
		this->array = new int [this->initial->size ()];
	
	}
	
	if ( this->test == NULL ) {
	
		this->test = new int [ this->dimensiune * this->dimensiune ];
	
	}
	
	if ( this->send == NULL ) {
	
		this->send = new std :: vector < std :: pair <int, std :: vector < std :: vector<int> > > > ();
		for ( unsigned int i = 0 ; i < this->initial->size () ; i ++ ) {
		
			std :: pair <int, std :: vector < std :: vector<int> > > p;
			p.first = this->initial->at(i).first;
			this->send->push_back ( p );
			
			std :: vector <int> V(this->initial->at(i).second.size () );
			std :: pair <int, std :: vector <int> > pp;
			pp.first = this->initial->at(i).first;
			pp.second = V;
			this->keep.push_back (pp);
		
		}
		
	}
	copyMatrix ( this->dimensiune * this->dimensiune , matrix , this->matrix );

}

/*
 * Destructorul clasei Keep : elibereaza memoria alocata.
 */
Keep :: ~Keep () {

	if ( this->matrix != NULL ) {
	
		delete [] this->matrix;
		this->matrix = NULL;
	
	}
	
	if ( this->array != NULL ) {
	
		delete [] this->array;
		this->array = NULL;
	
	}
	if ( this->test != NULL ) {
	
		delete [] this->test;
		this->test = NULL;
	
	}

}
#endif
