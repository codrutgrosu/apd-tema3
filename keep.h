/*
 * 
 * Codrut Cristian Grosu
 * codrut.cristian.grosu@gmail.com
 * Anul III - Seria CA - Grupa 333
 * Tema 3 - APD
 * 
 */
# ifndef __KEEP_H_
# define __KEEP_H_

# include <vector>
# include <iostream>
# include <fstream>


class Keep {

 private :

	std :: vector < std :: pair <int, std :: vector < std :: vector<int> > > >  *initial;
	
	std :: vector < std :: pair <int, std :: vector < std :: vector<int> > > > *send = NULL;
	
	std :: vector < std :: pair <int, std :: vector <int> > > theEnd;
	
	std :: vector < std :: pair < int , std :: vector < int > > > keep;
	
	int k, N, dimensiune, _succesor_exist, _is_valid;
	
	int *array = NULL, *matrix = NULL, *test = NULL;
	
	char *write;
	
	bool root;
	
	void putSquareInMatrix ( int &square , std :: vector <int> sol , int *matrix ,
			int &numThreads , int &n ) ;
	
	void copyMatrix ( int size , int * src , int * dest );
	
	bool checkSquareSudoku ( int square );
	
	void continueGenerating ();
	
	void stopGenerating ();
	
	void startGenerate ();
	
	int succesor ();
	
	int solutie ();
	
	int valid ();
	
	void init ();

 public :

	Keep ( std :: vector < std :: pair <int, std :: vector < std :: vector<int> > > >  * sol ,
		int *matrix , int &N);
	
	std :: vector < std :: pair <int, std :: vector < std :: vector<int> > > > * getSend ();
	
	void setWrite ( char * out );
	
	void endHomework ();
	
	void setRoot ();
	
	void clear ();
	
	int totalSquare();
	
	~Keep ();

};

# endif
