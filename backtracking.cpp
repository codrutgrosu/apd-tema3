/*
 * 
 * Codrut Cristian Grosu
 * codrut.cristian.grosu@gmail.com
 * Anul III - Seria CA - Grupa 333
 * Tema 3 - APD
 * 
 */
# ifndef __BACKTRACKING_CPP_
# define __BACKTRACKING_CPP_

# include "backtracking.h"
# include <string.h>

/*
 * Functia elibereaza o parte dintre vectorii folositi.
 */
void Backtracking :: clear () {
	
	array.clear ();
	solutions.clear ();
	constrangeri.clear ();
	translatare.clear ();
	
}


/*
 * Aici incepe algoritmul de generare al configuratiei unui patrat.
 */
void Backtracking :: start () {

	// Determin constrangerile care apar pentru intreaga matrice
	propagareaConstrangerilor ();

	// Determin patratul pentru care voi genera solutii
	getMySquare ();
	
	// pastrez doar constrangerile pentru patratul meu
	keep_constraints ();

	// Generez toate solutiile pentru patratul meu
	if ( this->constrangeri.size () != 0 ) {
	
		generateSolutions ();
		
	} else {
	
		this->solutions.push_back (this->array);
	
	}
	
	
}

/*
 * Functia returneaza numatul de solutii gasite pentru patratul corespunzator.
 */
int Backtracking :: numarSolutii () {

	return this->solutions.size ();

}

/*
 * Functia intoarce toate solutiile gasite pentru patratul corespunzator.
 */
std :: vector < std :: vector <int> > Backtracking :: getAllSolutions () {

	return this->solutions;

}

/*
 * Functia uptateaza configuratia initiala de sudoku in functie de constrangerile aparute.
 */
void Backtracking :: updateMatrix ( int *matrix ) {

	for ( int i = 0 ; i < this->dimensiune; i ++ ) {
	
		for ( int j = 0 ; j < this->dimensiune ; j ++ ) {
		
			matrix[i * this->dimensiune + j] = this->matrix[i * this->dimensiune + j];
		
		}
	
	}

}

/*
 * Functia identifica constrangerile si apoi le propaga.
 */
void Backtracking :: propagareaConstrangerilor () {

	int value, pos;
	int change = 1;
	std :: vector < std :: pair < int , std :: vector <int> > > :: iterator it;
	while ( change ) {
		
		this->constrangeri.clear ();
		for ( int i = 0 ; i < this->dimensiune ; i ++ ) {
	
			for ( int j = 0 ; j < this->dimensiune ; j ++ ) {
		
				pos = i*this->dimensiune + j;
				value = this->matrix[pos];
				if ( value == 0 ) {
			
					std :: pair <  int , std :: vector <int> > constr;
					constr.first = pos;
					row_column_constraints ( constr.second , pos );
					boxConstraints ( i , j , pos , constr.second );
					this->constrangeri.push_back (constr);
			
				}
		
			}
	
		}

		change = 0;
		it = this->constrangeri.begin ();
		for ( ; it != this->constrangeri.end () ; ) {
	
			if ( it->second.size () == 1 ) {
		
				change = 1;
				this->matrix[it->first] = it->second[0];
				this->keep[it->first] = 0;
				this->constrangeri.erase (it);
		
			} else {
		
				it ++;
		
			}
			
		}

	}

}

/*
 * Functia pastreaza constrangerile pentru patratul curent.
 */
void Backtracking :: keep_constraints () {

	std :: vector < std :: pair < int , std :: vector <int> > > :: iterator it;
	for ( it = this->constrangeri.begin () ; it != this->constrangeri.end () ; ) {
	
		if ( this->keep[ it->first ] == 0 ) {
		
			this->constrangeri.erase (it);
		
		} else {

			it ++;
		
		}
	
	}
	
	translateConstraints ();
	
}

/*
 * Functia translateaza pozitia constrangerilor in functie de patratul curent.
 */
void Backtracking :: translateConstraints () {
	
	std :: vector < std :: pair < int , std :: vector <int> > > :: iterator it;
	std :: vector < std :: pair <int,int > > :: iterator i;
	for ( i = this->translatare.begin () ; i != this->translatare.end () ; i++ ) {
	
		for ( it = this->constrangeri.begin () ; it != this->constrangeri.end () ; it ++ ) {
		
			if ( i->first == it->first ) {
			
				it->first = i->second;
			
			}
		
		}
		
	}

}

/*
 * Functia determina constrangerile in functie de valorile care exista deja in patrat.
 */
void Backtracking :: boxConstraints ( int &line, int & col , int &pos ,
					 std :: vector <int> & constr ) {

	// determin patratul in care sunt
	int  l = pos / ( this->N * this->dimensiune );
	int c = col / this->N;
	int pos_vecini, val;
	int start = l * this->dimensiune * this->N + this->N * c;

	for ( int i = 0 ; i < this->N ; i ++ ) {
	
		for ( int  j = 0 ; j < this->N ; j ++ ) {
	
			pos_vecini = i * this->dimensiune + start + j;
			val = this->matrix[pos_vecini];
			if ( pos != pos_vecini && val != 0 ) {
			
				std :: vector < int > :: iterator it = constr.begin ();
				for ( ; it != constr.end () ; ) {
				
					if ( *it == val ) {
					
						it = constr.erase (it);
					
					} else {
					
						++it;
					
					}
				
				}
			
			} 
			
		}
		
	}


}

/*
 * Functia selecteaza patratul pentru care se vor genera solutii.
 */
void Backtracking :: getMySquare () {

	int k = 0;
	int line = this->square / this->N;
	int column = this->square % this->N;
	int start = line * this->dimensiune * this->N + column * this->N;

	for ( int i = 0 ; i < this->N ; i ++ ) {
	
		for ( int  j = 0 ; j < this->N ; j ++ ) {
	
			int pos = i * this->dimensiune + start + j;
			int  x = this->matrix[ pos ] ;
			this->array.push_back ( x ) ;
			if ( x == 0 ) {
			
				this->keep[pos] = 1;
				std :: pair < int , int > trans;
				trans.first = pos;
				trans.second = k;
				translatare.push_back (trans);
			
			}
			k ++;
	
		}
		
	}

}

/*
 * Constructorul clasei Backtrack : aloca memorie si initializeaza variabilele.
 */
Backtracking :: Backtracking (int & N , int *matrix , int & square) {

	this->N = N;
	this->square = square;
	this->dimensiune = N * N;
	if ( this->matrix == NULL ) {
	
		this->matrix = new int [this->dimensiune * this->dimensiune];
	
	}
	if ( this->check == NULL ) {
	
		this->check = new int [this->dimensiune + 1];
	
	}
	if ( this->play == NULL ) {
	
		this->play = new int [this->dimensiune];
	
	}
	for ( int i = 0 ; i < this->dimensiune * this->dimensiune ; i ++ ) {
	
		this->matrix[i] = matrix[i];
	
	}
	
	if ( this->keep == NULL ) {
	
		this->keep = new int [this->dimensiune * this->dimensiune];
	
	}
	memset ( keep , 0 , sizeof (int) * this->dimensiune* this->dimensiune );

}

/*
 * Destructorul clasei Backtracking : elibereaza memoria.
 */
Backtracking :: ~Backtracking () {

	if ( this->matrix != NULL ) {
	
		delete [] this->matrix;
	
	}
	
	if ( this->play != NULL ) {
	
		delete [] this->play;
	
	}
	
	if ( this->check != NULL ) {
	
		delete [] this->check;
	
	}
	
	if ( this->keep != NULL ) {
	
		delete [] this->keep;
	
	}

}

/*
 * Functia reseteaza vectorul check la valoarea value.
 */
void Backtracking :: resetCheck ( int value ) {

	for ( int i = 1 ; i <= this->dimensiune  ; i ++ ) {
	
		this->check[i] = value;
	
	}

}

/*
 * Functia intoarce constrangerile in functie de vectorul check.
 */
void Backtracking :: returnCheckConstr ( std :: vector <int> & constr ) {

	for ( int i = 1; i <= this->dimensiune ; i ++ ) {
	
		if ( check[i] == 1 ) {
		
			constr.push_back ( i );
		
		}
	
	}

}

/*
 * Functia filtreaza constrangerile aparute in functie de linie si coloana.
 */
void Backtracking ::  row_column_constraints ( std :: vector < int > & constr , int & pos ) {

	int line = pos / this->dimensiune ,column = pos % this->dimensiune;
	int value1, value2;

	resetCheck (1);
	// constrangeri pentru linie si coloana
	for ( int i = 0 ; i < this->dimensiune ; i ++ ) {
	
		value1 = this->matrix[i + this->dimensiune * line];
		value2 = this->matrix[ column + i * this->dimensiune ];

		if ( value1 != 0 ) {
		
			this->check[value1] = 0;
		
		}
		
		if ( value2 != 0 ) {
		
			this->check[value2] = 0;
		
		}
	
	}
	returnCheckConstr ( constr );

}

/*
 * Functie ajutatoare pentru backtracking.
 */
void Backtracking :: init () {

	this->play[ this->constrangeri[this->k].first ] = -1;

}

/*
 * Functie ajutatoare pentru backtracking.
 */
int Backtracking :: succesor () {

	int size = this->constrangeri[this->k].second.size ();
	if ( this->play[ this->constrangeri[this->k].first ] < size - 1 ) {
	
		this->play[ this->constrangeri[this->k].first ] ++;
		return 1;
	
	}
	
	return 0;

}

/*
 * Functia verifica daca elementul curent poate sa apartina unei solutii.
 */
int Backtracking :: valid () {

	int value = this->constrangeri[k].second[ this->play[this->constrangeri[k].first] ];
	for ( int i = 0 ; i < k ; i ++ ) {
	
		if ( this->constrangeri[i].second[ this->play[ this->constrangeri[i].first ] ] == value )
			
			return 0;
	
	}

	return 1;

}


/*
 * Functia intoarce pozitia pe care se afla un element din patratul curent in matricia initiala.
 */
int Backtracking :: returnPos ( int x ) {

	for ( unsigned int i = 0 ; i < this->translatare.size () ; i ++ ) {
	
		if ( this->translatare[i].second == x ) {
		
			return translatare[i].first;
		
		}
	
	}
	
	return -1;

}

/*
 * Functia semnalizeaza cand s-a ajuns la o solutie.
 */
int Backtracking :: solutie () {

	int size = this->constrangeri.size ();
	return this->k + 1 == size;

}

/*
 * Functia principala care se ocupa cu generarea tuturor solutiilor pentru patratul curent.
 */
void Backtracking :: generateSolutions () {

	this->k = 0;
	this->init ();
	while ( k > -1 ) {
	
		this->_succesor_exist = 1;
		this->_is_valid = 0;
		
		while ( this->_succesor_exist == 1 && ! this->_is_valid ) {
	
			this->_succesor_exist = this->succesor ();
			if ( this->_succesor_exist ) {
			
				this->_is_valid = this->valid ();
			
			}
		
		}
		
		if ( this->_succesor_exist ) {
		
			if ( this->solutie () ) {

				this->addSolution ();
			
			} else {
			
				this->k ++;
				init ();
			
			}
		
		} else {
		
			this->k --;
		
		}
	
	}

}

/*
 * Functia intoarce solutia cu numarul de ordine x.
 */
std :: vector <int> Backtracking ::  getSolution ( int x ) {

	if ( this->solutions.size () == 0 ) {
	
		return this->array;
		
	}
	return this->solutions[x];

}

/*
 * Functia se ocupa de adaugarea unei solutii la vectorul de solutii.
 */
void Backtracking :: addSolution () {

	std :: vector <int> sol(this->dimensiune) ;
	for ( int i = 0 ; i < this->dimensiune ; i ++ ) {
				
		sol[i] = this->array[i];
				
	}
	for ( unsigned int j = 0 ; j < this->constrangeri.size () ; j ++ ) {
							
		sol[this->constrangeri[j].first] = 
		this->constrangeri[j].second [this->play[constrangeri[j].first ]];
							
	}
				
	this->solutions.push_back (sol);

}

# endif
