/*
 * 
 * Codrut Cristian Grosu
 * codrut.cristian.grosu@gmail.com
 * Anul III - Seria CA - Grupa 333
 * Tema 3 - APD
 * 
 */
# ifndef __BACKTRACKING_H_
# define __BACKTRACKING_H_

# include <iostream>
# include <fstream>
# include <vector>
# include <string>
# include <stdlib.h>
# include <sstream>
# include <iterator>


class Backtracking {
 
 private :
	
	std :: vector < std :: pair < int , std :: vector <int> > > constrangeri;
	
	int *matrix = NULL, *play = NULL, *check = NULL, *keep = NULL;
	
	int N, square, dimensiune, k, _succesor_exist, _is_valid;
	
	std :: vector < std :: pair <int,int> > translatare;
	
	std :: vector < std :: vector < int > > solutions;
	
	std :: vector <int> array;
	
	void boxConstraints ( int &line, int & col , int &pos , std :: vector <int> & constr );
	
	void row_column_constraints ( std :: vector < int > & constr , int & pos );
	
	void returnCheckConstr ( std :: vector <int> & constr );
	
	void propagareaConstrangerilor ();
	
	void translateConstraints ();
	
	void resetCheck (int value);
	
	void generateSolutions ();
	
	void keep_constraints ();
	
	int returnPos ( int x );
	
	void getMySquare ();
	
	void addSolution ();
	
	int succesor ();
	
	int solutie ();
	
	void init ();
	
	int valid ();
	
 public :
	
	std :: vector < std :: vector <int> > getAllSolutions ();

	Backtracking ( int & N , int *matrix, int & square );
	
	std :: vector <int> getSolution ( int x );

	void updateMatrix ( int *matrix );
	
	int numarSolutii ();
	
	~Backtracking ();
	
	void start ();
	
	void clear ();

};

# endif
